#!/bin/bash

# Objective:
# This script minimizes the file size of a set of SVG files (using Scour and Inkscape).
# It also creates a pdf file for each SVG file.


# Usage:
# Copy all the SVG files that you would like to convert into a directory 'to_convert',
# in the directory where the script is.
# Then run this script with `sh convert_svg.sh`. Find the SVG and PDF files in a
# subdirectory `processed`.

cd to_convert
mkdir -p processed

for i in *.svg
do
  bname=$(basename "$i" .svg)
  echo "$i --export-plain-svg=./processed/plain-$i" >> commands_svg.txt
  echo "./processed/$i --export-pdf=./processed/$bname.pdf" >> commands_pdf.txt
done

# my version of scour leaves a <sodipodi> element in - to get rid of that, first export as plain svg
inkscape --shell < commands_svg.txt

for i in *.svg
do
  # optimize the plain svg files with scour
  scour -i ./processed/plain-$i -o ./processed/$i --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none --set-precision=8
done

# delete them, not needed anymore
rm ./processed/plain-*.svg

# now convert all the svg files to pdf
inkscape --shell < commands_pdf.txt

rm commands_pdf.txt
rm commands_svg.txt
